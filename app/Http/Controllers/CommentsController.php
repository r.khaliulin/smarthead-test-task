<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    /**
     * Выводим все комментарии
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $comments = Comment::all();
        return view('comments.index', compact('comments'));
    }

    /**
     * Выводим конкретный комментарий
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
        $comment = Comment::find($id);
        return view('comments.show', compact('comment'));
    }

}
