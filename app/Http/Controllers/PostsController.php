<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    /**
     * Выводим все посты
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $posts = Post::all();
        return view('posts.index', compact('posts'));
    }

    /**
     * Выводим конкретный пост
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
        $post = Post::find($id);
        return view('posts.show', compact('post'));
    }

    /**
     * Добавляем комментарий к посту
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addComment(Post $post) {
        // Проверяем данные с формы
        $this->validate(request(), [
            'post_id' => 'required|integer|exists:posts,id',
            'comment' => 'required|min:5|max:100'
        ]);
        // Добавляем комментарий
        $post->addComment(request()->all());
        return back();
    }

}
