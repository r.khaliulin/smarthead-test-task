<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;

class AuthorsController extends Controller
{

    /**
     * Выводим всех авторов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $authors = Author::all();
        return view('authors.index', compact('authors'));
    }

    /**
     * Выводим конкретного автора
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
        $author = Author::find($id);
        return view('authors.show', compact('author'));
    }

}
