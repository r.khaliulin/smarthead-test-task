<?php

namespace App\Http\Controllers;

use App\Author;
use App\Post;
use Faker\Generator as Faker;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ParserController extends Controller
{

    /**
     * Стартуем парсинг
     */
    public function start(Faker $faker) {
        // Создаем клиент с указанием базового URL
        $client = new Client([
            'base_uri' => \Config::get('parser.base_url'),
            'verify' => false
        ]);
        // Получаем данные по интересующей нас ссылке
        $response = $client->request('GET', \Config::get('parser.news'));
        // Преобразуем ответ сервера в массив
        $json = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
        // Перебираем спарсенные новости
        if (!empty($json['documents'])) {
            foreach ($json['documents'] as $document) {
                // Получаем хэш автора
                $authorHash = !empty($document['source']['name']) ? md5($document['source']['name']) : false;
                // Получаем хэш поста
                $postHash = !empty($document['title']) ? md5($authorHash . $document['title']) : false;
                // Если успешно сформированы
                if ($authorHash && $postHash) {
                    // Добавляем автора или берем имеющегося
                    $author = Author::firstOrCreate(
                        ['hash' => $authorHash],
                        [
                            'hash' => $authorHash,
                            'email' => $faker->unique()->safeEmail,
                            'name' => strip_tags($document['source']['name'])
                        ]
                    );
                    $authorId = $author->id;
                    $author->save();
                    // Проверяем существование поста, если нет такого, то добавляем
                    if ($authorId) {
                        $post = Post::firstOrCreate(
                            ['hash' => $postHash],
                            [
                                'hash' => $postHash,
                                'author_id' => $authorId,
                                'post' => strip_tags($document['title'])
                            ]
                        );
                        $post->save();
                    }
                }
            }
        }
    }

}
