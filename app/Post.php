<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['hash', 'author_id', 'post'];

    /**
     * Возвращаем комментарии поста
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany(Comment::class);
    }

    /**
     * Возвращаем автора поста
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author() {
        return $this->belongsTo(Author::class);
    }

    /**
     * Добавляем комментарий
     * @param $data
     * @return int
     */
    public function addComment($data) {
        // Добавляем комментарий
        $comment = new Comment();
        $comment->fill($data);
        $comment->save();
        return $comment->id;
    }

}
