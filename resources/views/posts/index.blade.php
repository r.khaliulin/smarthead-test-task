@extends('layout')

@section('content')
    @forelse($posts as $post)
        @include('blocks.post')
    @empty
        @include('blocks.not_found', ['message' => 'Постов не найдено'])
    @endforelse
@endsection