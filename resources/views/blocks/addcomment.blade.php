<div class="col-lg-12 blog-post">
    <h3>Добавить комментарий</h3>
    <form method="POST" action="{{route('post.comment', [$post->id])}}">
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Введите текст комментария</label>
            <input type="hidden" name="post_id" value="{{$post->id}}" />
            <textarea name="comment" class="form-control" rows="3"></textarea>
            {{csrf_field()}}
        </div>
        <button type="submit" class="btn btn-primary mb-2">Добавить комментарий</button>
    </form>
</div>