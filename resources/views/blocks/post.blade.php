<div class="col-lg-12 blog-post">
    <h1 class="blog-post-title"><a href="{{route('post.show', [$post->id])}}">Пост {{$post->id}}</a></h1>
    <p>автор: <a href="{{route('author.show', [$post->author->id])}}">{{$post->author->name}}</a></p>
    <hr />
    <p class="lead">{{$post->post}}</p>
    <hr />
    @if(!$post->comments->isEmpty())
        <h3>Комментарии к посту</h3>
        <br />
        @foreach($post->comments as $comment)
            <div class="media mb-4">
                <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                <div class="media-body">
                    <h5 class="mt-0"><a href="{{route('comment.show', [$comment->id])}}">Комментарий {{$comment->id}}</a></h5>
                    <span>{{$comment->comment}}</span>
                </div>
            </div>
        @endforeach
    @else
        <h3>Отсутствуют комментарии</h3>
    @endif
</div>

@include('blocks.addcomment')