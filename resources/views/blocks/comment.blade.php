<div class="col-lg-12 blog-post">
    <h3><a href="{{route('comment.show', [$comment->post->author->id])}}">Комментарий {{$comment->id}}</a></h3>
    <p>
        <a href="{{route('post.show', [$comment->post->author->id])}}">Пост {{$comment->post->id}}</a>
        <br />
        <a href="{{route('author.show', [$comment->post->author->id])}}">Автор {{$comment->post->author->name}}</a>
    </p>
    <hr />
    <p class="lead">{{$comment->comment}}</p>
</div>