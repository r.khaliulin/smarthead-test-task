<div class="col-lg-12 blog-post">
    <h3><a href="{{route('author.show', [$author->id])}}">Посты автора {{$author->name}}</a></h3>
    <hr />
    @if(!$author->posts->isEmpty())
        @foreach($author->posts as $post)
            <h5 class="mt-0"><a href="{{route('post.show', [$post->id])}}">Пост {{$post->id}}</a></h5>
        @endforeach
    @else
        <h3>Отсутствуют посты у данного автора</h3>
    @endif
</div>