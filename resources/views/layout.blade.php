<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Тестовое задание</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/blog.css">
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('posts')}}">Все посты</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('authors')}}">Все авторы</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('comments')}}">Все комментарии</a>
            </li>
        </ul>
    </div>
</nav>
<main role="main" class="container">
    @include('blocks.error')
    @yield('content')
</main>
</body>

</html>
