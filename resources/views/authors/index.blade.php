@extends('layout')

@section('content')
    @forelse($authors as $author)
        @include('blocks.author')
    @empty
        @include('blocks.not_found', ['message' => 'Авторов не найдено'])
    @endforelse
@endsection