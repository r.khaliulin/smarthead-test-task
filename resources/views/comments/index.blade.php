@extends('layout')

@section('content')
    @forelse($comments as $comment)
        @include('blocks.comment')
    @empty
        @include('blocks.not_found', ['message' => 'Комментариев не найдено'])
    @endforelse
@endsection