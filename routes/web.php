<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Показываем все посты на главной
Route::get('/', 'PostsController@index')->name('posts');
// Показываем информацию по конкретному посту
Route::get('/posts/{id}', 'PostsController@show')->name('post.show');
// Добавляем комментарий к посту
Route::post('/posts/{id}/comment', 'PostsController@addComment')->name('post.comment');

// Показываем всех авторов
Route::get('/authors', 'AuthorsController@index')->name('authors');
// Показываем информацию по конкретному автору
Route::get('/authors/{id}', 'AuthorsController@show')->name('author.show');

// Показываем все комментарии
Route::get('/comments', 'CommentsController@index')->name('comments');
// Показываем информацию по конкретному комментарию
Route::get('/comments/{id}', 'CommentsController@show')->name('comment.show');