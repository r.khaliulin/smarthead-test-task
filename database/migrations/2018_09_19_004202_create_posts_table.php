<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{

    // Название таблицы
    private $tableName = 'posts';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id');
            $table->text('post');
            $table->char('hash',  32)->unique();
        });
        // Добавляем коммент к таблице
        DB::statement("ALTER TABLE `{$this->tableName}` COMMENT 'Список постов разных авторов'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
